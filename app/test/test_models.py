from django.test import TestCase
from ..models import *


class ProductTest(TestCase):
    """ Test module for Product model """


    def test_product_insertions(self):
        product = Product.objects.create(
            price=125.2, title='shirt', available_inventory=2)
        product.save()
        self.assertEqual(
            str(product), "shirt")