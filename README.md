## Content:
![](./readme-images/admin-dashboard.png)

* How to run the project on your computer
* API
    * Adding an item to the cart
    * Removing an item from the cart
    * Making a purchase
    * View a user's order history

## Installation / Run 

### Local on your computer
Step 1) Check that you are at the level that contains the env/ directory (the same level that also has this README file) and activate the virtual env by running:
    `source env/bin/activate`

    Possible issue: You do not have virtualenv. To install it run:
        `pip install virtualenv`
        Then try `source env/bin/activate` again.


Step 2) Go into the ecommercesite/ directory and install the dependencies by running:
    ```
    cd ecommercesite/
    pip install -r requirements.txt
    ```

Step 3) You should still be in the directory ecommercesite/ which contains the manage.py file.
    Start the server:
        `python manage.py runserver`

Step 4) Keep the server running on that tab and then open another terminal tab. Run the curl commands from the API section below in that tab.

Step 5) It's simpler to just use the curl commands, but you can also interact with the API
using the Django Rest interface. Just go to `http://localhost:8000/` ( or the port your development server is running on) in your browser. You'll need to login. The username is `admin` and the password is `password123`. The endpoints
to use the interface are below in the API section after each curl command.

### Docker
Step 1) install [Docker & Docker-compose](https://docs.docker.com/compose/install/)

Step 2) Run `docker-compose build`

Step 2) Run `docker-compose up`


## API

You can interact with the API either using curl commands in the terminal or the
Django Rest Framework interface. Both included in each section, but using
curl commands is simpler and faster.
![](./readme-images/api.png)

### Register 

Try this:
```
curl -X POST http://localhost:8000/register/ -d "username=omar&password=omar111&password2=omar111&email=omar@omar.com&first_name=omar&last_name=Omar" -H 'Accept: application/json;' 
 
```
or you can use RestFrameWork View
![](./readme-images/register.png)

### Login
you can login with this url `http://localhost:8000/api-auth/login/`

![](./readme-images/login.png)

### Adding Product
you can login with this url `http://localhost:8000/products/`

![](./readme-images/products.png)


### Adding an item to the cart

 - available_inventory must be > 0
 
Using Terminal

Keep the server running and on another tab use the curl command.

endpoint is `/carts/<card_id>/add_to_cart/` and you have to send the product id
of the product you want to add to the cart and the quantity.
Try this:

```
curl -X PUT http://127.0.0.1:8000/carts/1/add_to_cart/ -d "product_id=1&quantity=2" -H 'Accept: application/json; indent=4' -u admin:password123
```
Using Django Rest Interface

First login (username = `admin` password = `password123`) then go to:

`http://localhost:8000/carts/1/add_to_cart/`

If you scroll to the bottom you will see a section to make a PUT request.
In the content section add `{"product_id":1, "quantity"=2}` and hit the PUT button.

Seeing the serialized cart means the request was successful otherwise
you'll see
```
{
    "status": "fail"
}
```

To check things in the shell
go to the directory with manage.py file and then run `./manage.py shell`
```
>>> from shop.models import *
>>> u = User.objects.get(id=1)
>>> u.cart.items.all()
<QuerySet [<CartItem: CartItem object>]>
>>> u.cart.items.values_list('product__title','quantity')
<QuerySet [(u'tshirt', 2)]>
```

### Removing an item from the cart

Using Terminal

endpoint is `/carts/<card_id>/remove_from_cart/` and you have to send the product id
of the product you want to remove from the cart. Items are removed one by one so
the quantity is always 1.

Try this:

```
curl -X PUT http://127.0.0.1:8000/carts/1/remove_from_cart/ -d "product_id=1" -H 'Accept: application/json; indent=4' -u admin:password123
```

Using Django Rest Interface

`http://localhost:8000/carts/1/remove_from_cart/`

If you scroll to the bottom you will see a section to make a PUT request.
In the content section add `{"product_id":1}` and hit the PUT button.

Same as adding to the cart, removing from it the response will be the serialized cart, but it won't display the nested cart items because of how the relationship was handled in the
serializers.

To check things in the shell go to the directory with manage.py file and then run `./manage.py shell`
```
>>> from shop.models import *
>>> u = User.objects.get(id=1)
>>> u.cart.items.all()
<QuerySet [(u'tshirt', 1)]>
```


### Making a purchase

- available_inventory decrements by the appropriate amount
- purchases don't happen that would make available_inventory dip below 0:

Using Terminal

endpoint is `/orders/` and you have to send the purchaser id (the user id of the customer making the purchase/order).
An Order is created with the contents of the user's cart.

Try this:

```
curl -X POST -H "Content-Type:application/json; indent=4" -d '{"purchaser":1}' -u admin:password123 http://127.0.0.1:8000/orders/
```

Using Django Rest Interface


`http://localhost:8000/orders/`

If you scroll to the bottom you will see a section to make a POST request.
In the content section add:
```
{
    "total": null
    "purchaser": 1
}
```
Then hit the POST button.

To check things in the shell go to the directory with manage.py file and then run `./manage.py shell`
```
>>> from shop.models import *
>>> u = User.objects.get(id=1)
>>> u.orders.all()
<QuerySet [(u'tshirt', 1)]>
>>> u.orders.values_list('created_at')
<QuerySet [(datetime.datetime(2022, 6, 8, 2, 44, 27, 428748, tzinfo=<UTC>)]
>>> u.cart.items.all()
<QuerySet []>
```
Cart should be empty after creating an Order.

### View a user's order history

Using Terminal

endpoint is `/orders/order_history/?user_id=<user_id>`

Try this:

```
curl -X GET http://127.0.0.1:8000/orders/order_history/?user_id=1 -H 'Accept application/json; indent=4' -u admin:password123
```

Using Django Rest Interface

`http://localhost:8000/orders/order_history/?user_id=1`

You'll see the json for all of the user's orders:
![User Orger History](https://user-images.githubusercontent.com/8107962/26965408-7c2891d0-4caa-11e7-89b6-e4b721d1f6d1.png)


To check things in the shell go to the directory with manage.py file and then run `./manage.py shell`
```
>>> from shop.models import *
>>> u = User.objects.get(id=1)
>>> u.orders.all()
<QuerySet [<Order: Order object>]>
```
