#!/bin/sh
echo "Waiting for postgres..."

while ! nc -z postgres-ecommerce-backendecommerce 5432; do
  sleep 0.1
done

echo "PostgreSQL started"
python manage.py makemigrations app
python manage.py migrate
# python manage.py collectstatic
python manage.py createsuperuserwithpassword \
        --username omarmohamed1 \
        --password enter1234 \
        --email omarmohamed@example.org \
        --preserve

gunicorn --config gunicorn-cfg.py --reload project.wsgi
# python manage.py runserver 0.0.0.0:8000